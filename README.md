# PackageSorter

A CLI program that grabs the package list for a given repo and outputs the ten applications with the most packages.

## Steps taken when creating project

### Research

The first thing I always do is to ensure I have all of the necessary 
knowledge for creating the project.  I spent a couple of hours researching/
revising how to:

1. Create command line tools using *argparse*,
2. Get the regex working for grabbing the relevant info from the repo files,
3. Looked into progress bars, which I eventually didn't use as I wanted to
   restrict myself to the standard library.
4. Looked into the fastest sort algorithms for this task.

### Design

Before starting to code, I did some UML design work:

        # Package Sorter

        ## USE CASE Description

        1. The programs takes in a processor type.  The first line in a file is looaded with the URL.  A filename is created and used to download the file.  
        The file is unzipped.  The app names and counts are extracted from the unzipped file.  The names and counts are sorted and the top ten are 
        extracted.  The top ten is displayed.  The program ends.

        ## Triggers

        1. The user gives the name of a processor

        ## Actors

        1. CLI program
        2. Online file
        3. Count list
        4. Config file with URL and list of processors

        ## Preconditions

        1. A processor name is passed in

        ## Goals

        1. Pass back a list 

        ## Steps of Execution

        1. Check the Command is in the correct format.  If it is not, display a help
        message and exit the program.
        2. Check that the processor entered by the user is in the list.  If it is
        not in the list then output a list of valid processors and exit the program.
        3. Download the file with relevant error checking.  Allow the user to retry
        the download (or exit) if it did not work.
        4. Unzip the file
        5. Extract the repo app names and count their packages (package count list)
        6. Sort the package count list.
        7. Extract the top ten from the package count list
        8. print the top ten list
        9. exit the program

***Put images here***

### Implementation

Next, I created a gitlab repository and added a virtualenv and setup a 
basic folder structure (using https://github.com/navdeep-G/samplemod.git). 
I then built the main class structure from the design, including docstrings,
__repr__() methods and comments. 
