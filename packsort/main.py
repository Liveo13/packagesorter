import PSApp

def entry():
    psapp = PSApp.PSApp()
    psapp.run()

if __name__ == "__main__":
    print("Running packagesorter...\n")
    entry()

