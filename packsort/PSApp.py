from collections import Counter

import Command
import Configuration
import Repo

class PSApp():
    """Main class for the package sorter application."""
    FILENAME_PRE = 'Contents-'
    FILENAME_EXT = '.gz'

    configuration = Configuration.Configuration()
    command = Command.Command(configuration)
    repo = Repo.Repo()

    def __init__(self):
        """Intialises the PSApp class"""
        top_ten_packages :dict = Counter() 
    
    def run(self):
        """Main run routine for the whole package."""
        print("Downloading package list...\n")
        filename = self.FILENAME_PRE + self.command.processor + self.FILENAME_EXT
        self.repo.create_package_list(self.configuration.url,
                                      filename)
        print("Sorting the package list...")
        self.top_ten_packages = self.repo.get_top_ten()

        #print(self.top_ten_packages)
        print('\nTOP TEN PACKAGE COUNT\n---------------------')

        for count, item in enumerate(self.top_ten_packages):
            print(f'{count+1}. {item[0]}\nPackage Count = {item[1]}')

    def __repr__(self) -> str: 
        """Debugging print for PAApp class"""
        return f'\{self.top_ten_packages}'
