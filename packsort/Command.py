import logging

# FileManager borrowed from @SamyuktaSHegde
# https://www.geeksforgeeks.org/context-manager-in-python/

class FileManager():
    def __init__(self, filename, mode):
        self.filename = filename
        self.mode = mode
        self.file = None
          
    def __enter__(self):
        self.file = open(self.filename, self.mode)
        return self.file
      
    def __exit__(self, exc_type, exc_value, exc_traceback):
        self.file.close()

class Configuration():
    """Class to process and access the configuration file."""
    
    CONFIG_FILENAME = "packsort.cfg"

    def __init__(self) -> None:
        """Intialises the Configuration class"""
        url: str = ''
        valid_processors: list = []
        
        # Set up some basic logging
        logging.basicConfig(filename='logs/configuartion.log',level=logging.DEBUG)

        # Load the configurations (url and valid processors)
        try:
            with FileManager(self.CONFIG_FILENAME, 'rt') as f:
                self.url = f.readline().strip('\n')
                self.valid_processors = f.readline().split(' ')
                self.valid_processors[-1] = self.valid_processors[-1].strip('\n')
                logging.debug(f'{self.url=}')
                logging.debug(f'{self.valid_processors=}')

        except IOError as err:
            print("Error: can\'t find file or read data")
            print(err)
            exit()
        except Exception as err:
            print("Default Exception when requesting file: ")
            print(err)
            exit()

        assert self.url != '', 'URL not read in configuration initialization.'
        assert self.valid_processors != [], 'Valid processors not read in configuration initialization.'

    def check_processor(self, processor: str) -> bool:
        """Checks the processor is a valid type."""
        assert processor != '', 'Did not pass anything when checking if processor is valid.'

        if processor in self.valid_processors:
            return True
        return False

    def get_repo_url(self) -> str:
        """Gets the URL of the repo stored in the config file."""
        return self.url

    def __repr__(self) -> str:
        """Debugging print for Configuration class."""
        return f"""
        self.url = str({self.url})\n
        self.valid_processors = [{self.valid_processors}]
        """
