from collections import Counter

import gzip
import re
import requests

#LOCATION = 'http://ftp.uk.debian.org/debian/dists/stable/main/'
#FILENAME = 'Contents-amd64.gz'

class GzipManager():
    def __init__(self, filename, mode):
        '''Initialize file manager.  Need to check modes'''
        self.filename = filename
        self.mode = mode
        self.file = None

    def __enter__(self):
        self.file = gzip.open(self.filename, self.mode)
        return self.file

    def __exit__(self, exc_type, exc_value, exc_traceback):
        self.file.close()

    def __repr__(self):
        return f"FileManager(filename={self.filename},\
                            mode={self.mode},\
                            file={self.file}"


class Repo():
    """Class to get and process the repo package list file."""

    def __init__(self):
        """Intialises the Repo class"""
        self.package_list: dict = Counter()

    def create_package_list(self, url, filename):
        """Download the repo package list file and build it into a data structure that
        keeps a count of the number of packages per application."""

        # Do the download...
        try:
            r = requests.get(url + filename, allow_redirects=True)
            open(filename, 'wb').write(r.content)
        except requests.ConnectionError as err:
            print("Connection error when requesting file from the internet:")
            print(err)
            exit()
        except requests.exceptions.HTTPError as err:
            print("HTTP error when requesting file:")
            print(err)
            exit()
        except Exception as err:
            print("Default Exception when requesting file from the interweb: ")
            print(err)
            exit()

        # Now extract the file...
        try:
            with GzipManager(filename, 'rt') as f:
                # Go through the file, processing each line, one-by-one
                while True:
                    line = f.readline()
                    if not line:
                        break
                    components = re.findall("[^\s]+", line)
                    self.package_list[components[0]] = len(components[1].split(','))
            
        except IOError as err:
            print("Error: can\'t find file or read data")
            print(err)
            exit()
        except Exception as err:
            print("Default Exception when reading the downloaded file: ")
            print(err)
            exit()

    def get_top_ten(self) -> dict:
        """Sorts the package list data structure and return the top ten."""
        return self.package_list.most_common(10)

    def __repr__(self) -> str:
        """Debugging print for Repo class."""
        return "\{{self.package_list}}"
